{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": []
   },
   "source": [
    "# Hierarchical Chain Growth (HCG) - grow structural ensembles of IDPs/IDRs\n",
    "\n",
    "\n",
    "This Jupyter Notebook runs HCG to grow structural ensembles of arbitrary sequences of IDPs/IDRs by hierarchical assembly of randomly drawn dimer fragments from a presampled MD fragment library downloaded when creating the conda environment as described in 'binder/environment.yml' (from here: https://gitlab.mpcdf.mpg.de/MPIBP-Hummer/hcg-fragment-library.git. You may also use your own fragment library (make to adapt the arguments \"fragment_length\" and \"overlap\" to match the architecture of the fragments in your library - check https://github.com/bio-phys/hierarchical-chain-growth examples for suggestions how to construct such a fragment library)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## References\n",
    "\n",
    "- Pietrek L. M., Stelzl L. S., Hummer G., J. Chem. Theory Comput. 16(1), 725– 737 (2020), https://doi.org/10.1021/acs.jctc.9b00809\n",
    "    - for the underlying theory.\n",
    "- Pietrek L. M., Stelzl L. S., Hummer G., arXiv preprint arXiv:2210.16167 (2022), https://doi.org/10.48550/arXiv.2210.16167\n",
    "    - for a review of chain growth, and in particular the hierarchical implementation as used in HCG, to model the structural dynamics of flexible proteins in the context of recent advances in alternative computational approaches\n",
    "    \n",
    "Make sure to cite at least one of the publications above if you are using structural ensembles of disordered protein grown via HCG in your work."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Import modules - preliminaries"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import sys, os\n",
    "\n",
    "import itertools\n",
    "import warnings\n",
    "warnings.filterwarnings('ignore')\n",
    "\n",
    "from chain_growth.hcg_binder import run_hcg_binder as run_hcg"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Prepare HCG"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Input sequence\n",
    "\n",
    "As input sequence you can use the example fasta file uploaded here - the LCD of FUS, sequence as in Benayad et al. J. Chem. Theory Comput. 2021, 17, 1, 525–537.\n",
    "\n",
    "Or to grow models of a desired IDP or IDR paste your sequence as string below in one letter amino acid code.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "## example\n",
    "# sequence = 'fus_lcd_benayad.fasta'\n",
    "\n",
    "## user defined sequence\n",
    "sequence = 'MNQQQQQQQQQQQQQQQQQDALPMaaaAM'"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Definition of a few HCG arguments"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Define number of full-length chains you want to grow and the clash cut-offs for the assembly (= clash_distance < minimal distance between non-bonded atoms except H atoms). "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# maximal number of pairs/full-length models to assemble\n",
    "# recommended kmax values:\n",
    "## kmax = 50 - 10, in case you only want to grow a few starting models for MD\n",
    "## kmax >= 10000 in case you want to grow extensive ensembles. This may take a while.\n",
    "kmax = 200\n",
    "# cut-off distance in Angstrom for clash search, \n",
    "# clash_distance < minimal distance between non-bonded atoms except H atoms\n",
    "# it is safe to use the default value = 2.0\n",
    "# can be decrased to about 1.2 to dercease the global shape of the grown protein\n",
    "clash_distance = 2.0"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Run HCG"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "number_hcg_levels, path = run_hcg(sequence, kmax, clash_distance=clash_distance, streamlit_progressbar=None)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Simple analysis of the output structual ensemble"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## First we examine the output visually"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "path_full_length = '{}{}/0/'.format(path, number_hcg_levels)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "top = '{}pair0.pdb'.format(path_full_length)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "xtc = '{}pair.xtc'.format(path_full_length)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import MDAnalysis as mda\n",
    "from MDAnalysis.analysis import align\n",
    "import numpy as np"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "## load full-length models as MDAnalysis universe\n",
    "u = mda.Universe(top, xtc)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "mobile = u.trajectory[-1]\n",
    "ref = u.trajectory[0]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "## central third\n",
    "length_sequence = sequence.__len__()\n",
    "third = length_sequence / 3"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "## align structures on central third \n",
    "aligner = align.AlignTraj(u, u, select='resid {:.0f}:{:.0f}'.format(third, third*2), in_memory=True).run()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import nglview as nv "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#u_v = u.trajectory[:20]\n",
    "view = nv.show_mdanalysis(u)\n",
    "#view.add_representation('point', 'resname SOL')\n",
    "view.center()\n",
    "view "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Now we calculate some global properties"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Hydrodynamic radius\n",
    "\n",
    "We calculate the average hydrodynamic radius of the structural ensemble we have grown."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def calculate_average_hydrodynamic_radius_structure(unique_ca_distances_l, N, gamma=1, weights=None):\n",
    "    \"\"\"\n",
    "    R_h =  \\gamma N^2  / <\\sum_{i neq j} 1 / r_{i,j} > \n",
    "    \n",
    "    not sure about this gamma value..\n",
    "    \"\"\"\n",
    "    \n",
    "    sum_ca_dist_traj = np.array([np.sum(1./r[r>0]) for r in unique_ca_distances_l])\n",
    "    \n",
    "    if np.all(weights) == None:\n",
    "        av_ca_dist = np.average(sum_ca_dist_traj)\n",
    "    else:\n",
    "        av_ca_dist = np.average(sum_ca_dist_traj, weights=weights)\n",
    "       \n",
    "    return gamma * N**2 / av_ca_dist\n",
    "#     return( 1-1/ N) * av_ca_dist\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "\n",
    "out_ar_l = []\n",
    "\n",
    "## n residues in query protein\n",
    "n = u.residues.n_residues\n",
    "\n",
    "result_ar = np.zeros(int(n*(n-1)/2))\n",
    "\n",
    "#for ts in pyprind.prog_bar(u.trajectory):\n",
    "for ts in u.trajectory:\n",
    "    ca_atoms_pos = u.select_atoms(\"name CA\")\n",
    "    out_ar = mda.analysis.distances.self_distance_array(ca_atoms_pos.positions,\n",
    "                                                        result=result_ar, \n",
    "                                                        box=ts.dimensions)\n",
    "    out_ar_l.append(out_ar)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "calculate_average_hydrodynamic_radius_structure(out_ar_l, n) ## in  Angstrom"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Pairwise RMSD (RMSD$_{S_i , S_j}$)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import mdtraj as md"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "t = md.load(xtc, top=top)\n",
    "# t=t[:1000]\n",
    "# print(t)\n",
    "\n",
    "distances = np.zeros((t.n_frames, t.n_frames))\n",
    "for i in range(t.n_frames):\n",
    "    distances[i] = md.rmsd(t, t, i)\n",
    "# print(distances , distances.shape)\n",
    "\n",
    "## distances == symmetric matrix\n",
    "## to exlude doubled values (pairwise rmsd conformation 0 to 1  and 1 to 0 is the same)\n",
    "## and the pairwise rmsd of a conformation to itself (=0)\n",
    "## we need to extract the upper half of the symmetric matrix\n",
    "## == extract the values ABOVE THE DIAGONAL\n",
    "RMSDsisj = distances[np.triu_indices(distances.shape[0], k=1)] \n",
    "# print(RMSDsisj, RMSDsisj.shape)\n",
    "mean_RMSDsisj = np.average(RMSDsisj)\n",
    "print('Mean pairwise RMSD: {:.1f} nm'.format(mean_RMSDsisj))\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Plot RMSD$_{S_i , S_j}$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import matplotlib.pyplot as plt"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, ax = plt.subplots(figsize=(4,3))\n",
    "ax.hist(RMSDsisj, density=True)\n",
    "ax.axvline(mean_RMSDsisj, c='C1', label=r'av. RMSD$_{S_i , S_j}$')\n",
    "ax.set(xlabel=(r'RMSD$_{S_i , S_j}$ [nm]'), ylabel=(r'P(RMSD$_{S_i , S_j}$)'))\n",
    "plt.legend()\n",
    "plt.tight_layout()\n",
    "! mkdir -p $path/plots\n",
    "fig.savefig('{}plots/pairwiseRMSD.pdf'.format(path))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Radius of gyration ($R_G$)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "s = u.select_atoms('name CA')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "Rg = [s.atoms.radius_of_gyration() for ts in u.trajectory]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "Rg = np.asarray(Rg)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "mean_Rg = np.average(Rg)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print('Mean pairwise Rg: {:.1f} nm'.format(mean_Rg/10))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Plot $R_G$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, ax = plt.subplots(figsize=(4,3))\n",
    "ax.hist(Rg/10, density=True)\n",
    "ax.axvline(mean_Rg/10, c='C1', label=r'av. Rg')\n",
    "ax.set(xlabel=(r'Rg [nm]'), ylabel=(r'P(Rg)'))\n",
    "plt.legend()\n",
    "plt.tight_layout()\n",
    "fig.savefig('{}plots/Rg.pdf'.format(path))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Download the output"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!tar cfz out.tar.gz $path"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.18"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
