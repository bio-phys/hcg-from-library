---
# Feel free to add content and custom Front Matter to this file.
# To modify the layout, see https://jekyllrb.com/docs/themes/#overriding-theme-defaults

layout: home
---

# Welcome to the Hierarchical Chain Growth (HCG)!

Use the HCG web application to grow structural ensembles of intrinsically disordered proteins or regions (IDPs/IDRs) via fragment assembly.

---

# Launch web application to grow small to medium-sized ensembles
[HCG web application](https://notebooks.mpcdf.mpg.de/binder/v2/git/https%3A%2F%2Fgitlab.mpcdf.mpg.de%2Fbio-phys%2Fhcg-from-library.git/webapp?urlpath=streamlit)

---

## Description
In HCG, we grow conformations of disordered proteins by hierarchically assembling short fragments. The web application of HCG randomly draws dimer fragment conformations from a pre-generated MD library. HCG is installed from GitHub - check [here](https://github.com/bio-phys/hierarchical-chain-growth/tree/multiprocessing_hcg) for full documentation.

### When to use the HCG web application

Launching this application, the code is run on a MPCDF cluster with a limited run time of ~ 1-2 hours. Thus, for growing ensembles of <=5000 conformations the web application is perfectly suited. 

### Run HCG locally

In case you intend to grow extensively large ensembles (with >>5000 members) we recommend running HCG locally by, e.g., downloading and installing HCG from [here](https://gitlab.mpcdf.mpg.de/bio-phys/hcg-from-library). You can follow the instructions in the header of binder/environment.yml to install HCG in a conda environment (make sure to uncomment the entries for jupyter-proxy modules and mkl* modules). We provide a Jupyter Notebook for running HCG. For very extensive calculations this Notebook could be uploaded to another Cloud Service, e.g., Google Collab.

In the pre-sampled fragment library downloaded when installing HCG as noted above, we do only cover the 20 natural amino accids combined in every 20**2=400 possible dimeric combination with a chemical capping group at the termini. It is, however, possible to use your own fragment library as input for HCG. If you plan to do so make sure to make the following modifications to the provided Jupyter Notebook. Please note: If you have included a non-natural amino acid, you have to download the HCG scripts from [GitHub](https://github.com/bio-phys/hierarchical-chain-growth/tree/multiprocessing_hcg) and adapt the accepted amino acids (in chain_growth/fragment_list.py) and then re-install HCG (or adapt hcg_binder.py to import functions from the correct file). 

```python
path0 = "<path_to_your_input_fragment_library>"
number_hcg_levels, path, _ex = run_hcg(sequence, kmax, path0=path0, online_fragment_library=False)
```

Make sure to hit "run_hcg?" to check for more arguments you may want to adapt. 

You can also run the Streamlit-based application locally in case you find it more appealing using the following command:
```bash
streamlit run hcg_app.py
```

Please check the "hcg_app.py" script to check for more _advanced_ options you can run HCG with.

---

<font size="1">

## DISCLAIMER
### DECLARATION OF LIABILITY ### 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS “AS IS” AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

### Safety of personal data ### 

We value your privacy. No personal data is recorded or passed to third parties. We do not make use of your input data, nor of your output data.
All user session data is temporary and gets deleted after the session has ended. There is no backup of any of your data. 


</font>
