#/usr/bin/env python3

import streamlit as st
import warnings, os, getpass, shutil
import zipfile, zlib
warnings.filterwarnings('ignore')
import numpy as np
from chain_growth.hcg_binder import run_hcg_binder as run_hcg
from chain_growth.hcg_binder import estimate_run_time


def on_binder():
    return getpass.getuser() == "jovyan"

def quit_binder_webapp():
    """Shut down a session running within a Docker container on Binder."""
    os.system("skill -u jovyan")

# zip-directory function inspired from <https://stackoverflow.com/a/1855118>
def zipdir(path, zip_fh):
    for root, dirs, files in os.walk(path):
        for file in files:
            zip_fh.write(
                os.path.join(root, file),
                os.path.relpath(os.path.join(root, file),
                                os.path.join(path, '..')
                                )
            )



st.set_page_config(
    page_title="HCG",
    layout="wide")

st.title('Structural ensembles of disordered proteins via fragment assembly')
st.header("Hierarchical Chain Growth (HCG)")
st.markdown("Grow structural ensembles of intrinsically disordered proteins or regions (IDPs/IDRs)"
            " using a hierarchical implementation of a Monte Carlo chain growth algorithm - HCG,"
	    " [source code](https://github.com/bio-phys/hierarchical-chain-growth)."
            " The code runs on a MPCDF cluster with a limited run time of ~ 1-2 hours."
            " In case you do not plan to grow excessively large"
            " ensembles (>> 5000 structures) this web application is perfectly suited for you"
            " to run HCG without having to install anything locally. "
	    " For a more detailed insight into HCG, please check our publications (and cite if you use HCG):"
	    " [Hierarchical Ensembles of Intrinsically Disordered Proteins at Atomic Resolution in Molecular Dynamics Simulations](https://pubs.acs.org/doi/abs/10.1021/acs.jctc.9b00809)"
	    " and [Structural ensembles of disordered proteins from hierarchical chain growth and simulation](https://arxiv.org/abs/2210.16167).")

sequence = st.text_input("IDP/IDR input sequence in one-letter amino acid code.", key="sequence")

col, buff = st.columns([1,2])
kmax = col.number_input("Number of output models of your protein of interest, min. value should be 50.", 
                        key="kmax", min_value=50, value=50, format="%i")

if st.button("Run time"):
    estimated_run_time = estimate_run_time(np.vstack([kmax, len(sequence)]))
    st.write(f"Estimated run time in minutes: {estimated_run_time/60}. The calculated run time may overestimate the true run time for a few seconds or minutes")
    if estimated_run_time/60 > 100:
        st.write("NOTE: Your query is close to or exceeds the run time limit of ~ 2 hours. Your job is likely to be stopped before it finishes.")


mode = st.radio(label="HCG parameters", options=('Default (recommended)', 'Advanced'))
if mode == 'Default (recommended)':
    st.write("Run HCG with the default parameters and grow the protein from a dimer fragment library")
else:
    st.write("You can choose the cut-off distance for clash search. This is the minimal allowed distance " 
             "between non-bonded heavy atoms to not be counted as atom clash. We suggest to use "
             "the default value of 2.0 A. You can decrease this value to a minimum of ~ 1.2 A in order to "
             "decrease the global shape of the grown protein.")
    col, buff = st.columns([1,2])
    clash_distance = col.number_input("Cut-off for the clash distance (in Angstrom).",
                                      key="clash_distance", format="%f", min_value=1.2, value=2.0)
    st.write("You may want to adapt the RMSD cut-off for the heavy atom superimposition between fragments that are assembled, e.g., to allow for more flexibility. "
             "The default with 0.6 Anstrom is rather  strict.")
    col, buff = st.columns([1,2]) 
    rmsd_cut_off = col.number_input(label='RMSD cut-off for fragment alignment', min_value=0.3, value=0.6,
                                   format='%f', help="Cut-off for the RMSD of the aligned fragments")
    

    online_fragment_library = st.checkbox(label='Use online fragment library', help="In case you prepared"
                " your own fragment library following "
                "the instruction on gibthub () select 'False'. If 'True' HCG grows the protein from the fragment"
                " library that is available online (so far only a dimer fragment library)", value=True)  
    if online_fragment_library == False:
        path0 = st.text_input(label='Absolute path to the fragment library') 
        capping_groups = st.checkbox(label='Fragments in  the library are capped with a chemical group or not', 
                                  value=True)
    else:
        path0 = 'dimerLibrary/'
        capping_groups = True

    fragment_length = st.number_input(label='Fragment Length', min_value=2, value=2,  format="%i",
                                      help="In case you are not using the online fragment library"
                                      " you need to put here the fragment length of the fragments you"
                                      " used to prepare your own fragment library. "
                                      "  Else for use with the online fragment library: "
                                      "At the moment only the dimer fragment library is online.")
    overlap = st.number_input(label='Residue overlap between the subsequent fragments', 
                                  min_value=0, value=0, format="%i",
                                  help="In case you are not using the online fragment library"
                                      " you need to put here the overlap between subsequent fragments you"
                                      " used to prepare your own fragment library."
                                      "  Else for use with the online fragment library: "
                                      "At the moment only the dimer fragment library is online with no "
                                      "residue overlap between the fragments.")
    if not online_fragment_library and (fragment_length != 2 or overlap != 0):
        st.write("Currently only the dimer fragment library is uploaded. "
                "There we automatically set the fragment length = 2 with no residue overlap"
                " (overlap = 0)")
        fragment_length = 2
        overlap = 0
    verbose = st.checkbox(label="Verbose", help=" Let the HCG function print out information about"
                          " the alignment procedure in each level. Stdout is printed in your terminal.", value=False)

number_hcg_levels = None


if st.button("Run HCG"):
    bar = st.progress(0)
    if mode == 'Default (recommended)':
        number_hcg_levels, out_path = run_hcg(sequence, kmax, path='out/', streamlit_progressbar=bar)
    else:
        number_hcg_levels, out_path = run_hcg(sequence, kmax, path='out/', 
                        fragment_length=fragment_length, overlap=overlap,
                        capping_groups=capping_groups, 
			 clash_distance=clash_distance,
                        online_fragment_library=online_fragment_library,
                         rmsd_cut_off=rmsd_cut_off, streamlit_progressbar=bar,
                        verbose=verbose
			)




if number_hcg_levels is not None:
    with open("out/README", "w") as f:

        f.write("This output folder contains all pairs of fragments / pairs of pairs"
                " that have been assembled per hierarchical level"
                " (levels = 1...M, M = number assembly levels)."
                " Atom positions of assembled models are saved to pair0.pdb as topology and"
                " to pair.xtc as a trajectory file containing atom positions of the entire ensemble,"
                " with n_frame = n_model. The ensemble of the assembled full-length IDP are saved"
                " in the folder created in the last level, i.e., 'M/0/' .")
#    out_path_zip = 'out.zip'
#    
#    with zipfile.ZipFile(out_path_zip, 'w',
#                         compression=zipfile.ZIP_DEFLATED, 
#                         compresslevel=1) as zip_fh:
#        zipdir(out_path, zip_fh)
#    zipfile = out_path_zip
#    with open(zipfile, "rb") as f:
#        data = f.read()
#    size = os.path.getsize(zipfile) / 1024. / 1024.
#    st.download_button(label="Download HCG ouput ({:.1f} MB)".format(size),
#            help="Download the complete output data as a zip file.",
#            data=data,
#            file_name=out_path_zip,
#            mime="application/zip")
#
#    if os.path.isdir("out.zip"):
#        shutil.rmtree("out")

