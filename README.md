# hcg-from-library

IDPs/IDRs grown via hierarchical assembly of dimer fragments.
Beta version of a HCG web application via Binder.

# Contributions

The code is implemented as described in Pietrek et al., JCTC, 2020, but using dimer fragments as input.
Fragment simulations and initial test were performed by Johannes Betz.
Code development: Lisa M. Pietrek, Lukas S. Stelzl, Johannes Betz.
Set-up of the Streamlit web application launched within a Binder enabled MPCDF Jupyter Notebook together with Klaus Reuter and Sebastian Kehl from the MPCDF.
Special thanks to Dr. Jürgen Köfinger for fruitful discussions and general support.

