#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Feb 28 14:40:55 2024

@author: Lisa
"""

import streamlit as st 
import os, getpass, shutil
import MDAnalysis as mda

   
class mdanalysis:
    
    def __init__(self, path_full_length):
        self.path_full_length = path_full_length
        top = '{}pair0.pdb'.format(path_full_length)
        xtc = '{}pair.xtc'.format(path_full_length)
        self.u = mda.Universe(top, xtc)
        
    def align(self):
        from MDAnalysis.analysis import align    
        u = self.u
        ## central third
        length_sequence = u.residues.n_residues
        third = length_sequence / 3
        ## align structures on central third 
        aligner = align.AlignTraj(u, u, select='resid {:.0f}:{:.0f}'.format(third, third*2),
                                  in_memory=True).run()
        
        _align = [u.atoms.write("{}/mol{}.pdb".format(path_full_length, i)) 
                  for i, ts in enumerate(u.trajectory[:6])]
        
    def end2end_distance(self):
        u = self.u
        nter = u.select_atoms("name N")[0]
        cter = u.select_atoms("name C")[-1]
        end_to_end_l = [np.linalg.norm(nter.position - cter.position)
                        for i, ts in enumerate(u.trajectory[:6])]
        return end_to_end_l
                       
def on_binder():
    return getpass.getuser() == "jovyan"

def quit_binder_webapp():
    """Shut down a session running within a Docker container on Binder."""
    os.system("skill -u jovyan")

    
    
st.header("Visualize models grown with HCG")


viz_options = ("None", "cartoon", "atomic_detail")
viz_select = st.selectbox("Show models as", viz_options, index=0)
if viz_select == "None":
    enable_viz = False
else:
    enable_viz = True

if not enable_viz:
    st.write("Make sure to run HCG first.")
else:    
    
    import numpy as np
    from stmol import showmol
    import py3Dmol
    
    width=1200
    height=800
    
    last_level = max([x for x in os.listdir("./out/") if not (x.startswith("c") or x.startswith("R"))])
    path_full_length = 'out/{}/0/'.format(last_level)
    mdanalysis(path_full_length).align()
    end_to_end_l = mdanalysis(path_full_length).end2end_distance()
    
    
    if viz_select == "cartoon":
        sel = "cartoon"
        colorstyle = {"color": "spectrum"}
    elif viz_select == "atomic_detail":
        sel = "stick"
        colorstyle = {'colorscheme': "grayCarbon"}
    
    #######################
    ### multiple viewers arrayed in a grid ###
    ######################
    
    
    view = py3Dmol.view(width=width, height=height, linked=False,viewergrid=(2,3))
    pi = 0
    pj = 0
    for i in range(6):
        m = "out/{}/0/mol{}.pdb".format(last_level, i)
        view.addModel(open(m, 'r').read(), format='pdb', viewer=(pi, pj)) 
        view.setStyle({sel: colorstyle})   
        view.addLabel("Model "+str(i), {"position": {"x":0, "y":0.5, "z":0}, 
                                        "useScreen": True, "font":'sans-serif',
                                        "fontSize":18,"fontColor":'white',"fontOpacity":1,
                                        "borderThickness":1.0, "borderColor":'red', "borderOpacity":0.5,
                                        "backgroundColor": 0x800080, "backgroundOpacity": 0.5},
                      viewer=(pi, pj))
        
        view.addLabel("End-to-end in Angstrom: {:.0f}".format(end_to_end_l[i]),
                      {"font":'sans-serif',"fontSize":18,"fontColor":'white',"fontOpacity":1,
                       "borderThickness":1.0, "borderColor":'red',
                       "borderOpacity":0.5, "backgroundColor":'black', "backgroundOpacity":0.5,
                       "position":{"x":100.0,"y":0.0,"z":0.0}, "useScreen": True},
                      viewer=(pi, pj))
    
        pj += 1
        if i == 2:
            pi += 1
            pj = 0
        
    
    view.zoomTo()
    view.setBackgroundColor('white')
    showmol(
        view,
        width=width,
        height=height)
        
with st.sidebar:
    if on_binder():
        label = "Shut Down Web Application"
        if st.button(label, help="Pushing this button shuts down the webapp, and you may close the browser tab."):
            
            shutil.rmtree("out")
            quit_binder_webapp()
